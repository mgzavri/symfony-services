<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EgrulData extends AbstractController
{

    /**
     * @throws GuzzleException
     */
    public function getData(string $apikey, string $inn) : array
    {

        $client = new Client();
        $url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party';

        $request = $client->post(
            $url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Token ' . $apikey
                ],
                'body'=>'{"query":"'.$inn.'","count":25}'
            ]
        );

        return json_decode($request->getBody()->getContents(), true);

    }


}
