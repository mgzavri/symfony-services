<?php
/**
 * Проверка вхождения координат в geojson файл
 * Файлы называются по слагам городов. например: mskgeo.json, spbgeo.json
 */

namespace App\Service;

use Symfony\Component\HttpKernel\KernelInterface;

class ZoneFinder
{

    private mixed $geoDirectory;
    private string $projectDir;

    public function __construct(KernelInterface $kernel, $geoDirectory)
    {
        $this->geoDirectory = $geoDirectory;
        $this->projectDir = $kernel->getProjectDir();
    }

    public function find(string $citySlug, array $point): ?int
    {
        $geoPath = $this->getPath($citySlug);
        $geo = json_decode(file_get_contents($geoPath), true);
        $zones = $geo['features'];
        foreach ($zones as $zone) {
            $zoneId = $zone['properties']['description']['id'];
            $coords = $zone['geometry']['coordinates'][0];
            if ($this->checkPolygone($coords, $point)) {
                return $zoneId;
            }
        }
        return null;
    }

    private function checkPolygone(array $coords, array $point): bool
    {
        $polyX = [];
        $polyY = [];
        foreach ($coords as $eachZoneLatLongs) {
            $polyX[] = $eachZoneLatLongs[1];
            $polyY[] = $eachZoneLatLongs[0];
        }
        $points_polygon = count($polyX);
        return $this->isInPolygone($points_polygon, $polyX, $polyY, $point);


    }

    private function isInPolygone(int $points_polygon, array $polyX, array $polyY, array $point): bool
    {
        $i = $j = $c = 0;
        for ($i = 0, $j = $points_polygon - 1; $i < $points_polygon; $j = $i++) {
            if ((($polyY[$i] > $point[1]) != ($polyY[$j] > $point[1])) &&
                ($point[0] < ($polyX[$j] - $polyX[$i]) * ($point[1] - $polyY[$i]) / ($polyY[$j] - $polyY[$i]) + $polyX[$i]))
                $c = !$c;
        }
        return $c;
    }

    private function getPath(string $citySlug): string
    {
       return $this->projectDir . $this->geoDirectory . '/' . $citySlug . 'geo.json';
    }

}